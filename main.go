package main

import (
	"encoding/hex"
	"flag"
	"log"
	"net/http"
	"os"
	"testApp/model"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	toml "github.com/pelletier/go-toml"
)

func main() {

	logger := log.New(os.Stdout, "", log.LstdFlags|log.Lshortfile)

	configPath := flag.String("config", "./config.toml", "path to config file")
	flag.Parse()

	configContent, err := toml.LoadFile(*configPath)
	if err != nil {
		logger.Fatalf("%+v\n", err)
	}

	config := Config{}
	if err := configContent.Unmarshal(&config); err != nil {
		logger.Fatalf("%+v\n", err)
	}

	sessionAuthkey, err := hex.DecodeString(config.SessionAuthKey)
	if err != nil {
		logger.Fatalf("%+v\n", err)
	}
	sessionEnckey, err := hex.DecodeString(config.SessionEncKey)
	if err != nil {
		logger.Fatalf("%+v\n", err)
	}
	store := sessions.NewCookieStore(sessionAuthkey, sessionEnckey)

	db, err := model.New(config.DSN)
	if err != nil {
		logger.Fatalf("%+v\n", err)
	}

	// dependency injections for our app
	app := App{
		logger:       logger,
		storage:      db,
		config:       &config,
		sessionStore: store,
	}

	router := mux.NewRouter().StrictSlash(true)
	apiRouter := router.PathPrefix("/api/v1").Subrouter()

	apiRouter.Handle("/user/login", app.errorMiddleware(app.login)).Methods("POST")
	apiRouter.Handle("/user/login/callback", app.errorMiddleware(app.loginOAuth))
	apiRouter.Handle("/user/forgot/create", app.errorMiddleware(app.createForgot)).Methods("POST")
	apiRouter.Handle("/user/forgot", app.errorMiddleware(app.forgot)).Methods("POST")
	apiRouter.Handle("/user/{email}", app.errorMiddleware(app.readUser)).Methods("GET")
	apiRouter.Handle("/user/{email}/profile", app.errorMiddleware(app.updateProfile)).Methods("POST")
	apiRouter.Handle("/user", app.errorMiddleware(app.createUser)).Methods("POST")

	router.HandleFunc("/forgot/{email}/{token}", app.webForgot).Methods("GET")
	router.HandleFunc("/profile", app.viewProfile).Methods("GET")
	router.HandleFunc("/profile/edit", app.editProfile).Methods("GET")
	router.HandleFunc("/{template}", app.home).Methods("GET")
	router.HandleFunc("/", app.home)

	// sane defaults in case anyone want to run bare go
	srv := http.Server{
		Addr:         config.Bind,
		Handler:      router,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	logger.Fatal(srv.ListenAndServe())
}
