package main

import (
	"net/http"
	"testApp/model"

	"github.com/gorilla/mux"
)

// any "controller" which renders template lives here
// data functions as support to send to each template
type data struct {
	model.User
	Error string
	*Config
}

func (app *App) home(w http.ResponseWriter, r *http.Request) {

	if status, err := app.getAuth(r); status && err == nil {
		http.Redirect(w, r, "/profile", http.StatusFound)
		return
	}

	vars := mux.Vars(r)
	templateFile := vars["template"]
	if templateFile == "" {
		templateFile = "signin"
	}

	render(w, templateFile, app.config)
}

func (app *App) webForgot(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	email, ok := vars["email"]
	if ok && email == "" {
		render(w, "error", data{Error: "missing input email"})
	}
	token, ok := vars["token"]
	if ok && token == "" {
		render(w, "error", data{Error: "missing input token"})
	}

	if auth, err := app.getAuth(r); err == nil && auth {
		http.Redirect(w, r, "/profile", http.StatusFound)
		return
	} else if err != nil {
		render(w, "error", data{Error: "whoops something went wrong"})
		app.logger.Printf("%+v\n", err)
		return
	}

	user := model.User{Email: email, Forgot: token}
	render(w, "setpass", data{User: user, Config: app.config})
}

func (app *App) viewProfile(w http.ResponseWriter, r *http.Request) {

	if auth, err := app.getAuth(r); err == nil && !auth {
		http.Redirect(w, r, "/signin", http.StatusFound)
		return
	} else if err != nil {
		render(w, "error", data{Error: "whoops something went wrong"})
		app.logger.Printf("%+v\n", err)
		return
	}

	email, err := app.getEmail(r)
	if err != nil {
		render(w, "error", data{Error: "whoops something went wrong"})
		app.logger.Printf("%+v\n", err)
		return
	}

	user, err := app.storage.ReadUserByEmail(email)
	if err != nil {
		render(w, "error", data{Error: "whoops something went wrong"})
		app.logger.Printf("%+v\n", err)
		return
	}

	render(w, "viewProfile", data{User: user, Config: app.config})
}

func (app *App) editProfile(w http.ResponseWriter, r *http.Request) {

	type data struct {
		model.User
		Error string
		*Config
	}

	if auth, err := app.getAuth(r); err == nil && !auth {
		http.Redirect(w, r, "/signin", http.StatusFound)
		return
	} else if err != nil {
		render(w, "error", data{Error: "whoops something went wrong"})
		app.logger.Printf("%+v\n", err)
		return
	}

	email, err := app.getEmail(r)
	if err != nil {
		render(w, "error", data{Error: "whoops something went wrong"})
		app.logger.Printf("%+v\n", err)
		return
	}

	user, err := app.storage.ReadUserByEmail(email)
	if err != nil {
		render(w, "error", data{Error: "whoops something went wrong"})
		app.logger.Printf("%+v\n", err)
		return
	}
	render(w, "editProfile", data{User: user, Config: app.config})
}
