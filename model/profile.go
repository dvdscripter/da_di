package model

import (
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

// Profile stores supplementary information about each user
type Profile struct {
	gorm.Model
	UserID    uint
	Name      string
	Address   string
	Telephone string
	Email     string
	User      *User
}

func (db *DB) UpdateProfile(email string, p Profile) (User, error) {
	dbUser, err := db.ReadUserByEmail(email)
	if err != nil {
		return User{}, err
	}

	if dbUser.Email != p.Email {
		dbUser.Email = p.Email
		if err := db.Save(&dbUser).Error; err != nil {
			return User{}, errors.Wrap(err, "cannot update user profile")
		}
	}

	if err := db.Model(dbUser.Profile).Update(p).Error; err != nil {
		return User{}, errors.Wrap(err, "cannot update user profile")
	}

	return dbUser, nil
}
