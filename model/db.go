package model

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
)

type DB struct {
	*gorm.DB
}

// Storage will allow us to swap db maintaining same interface
type Storage interface {
	Login(User) (bool, error)
	CreateUser(User) (User, error)
	ReadUser(uint) (User, error)
	ReadUserByEmail(string) (User, error)
	UpdateUser(uint, User) (User, error)
	NewForgot(id uint) (string, error)
	Profile(*User) (Profile, error)
	UpdateProfile(email string, profile Profile) (User, error)
}

func New(source string) (*DB, error) {
	db, err := gorm.Open("mysql", source)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot open db %s", source)
	}
	db.AutoMigrate(&User{}, &Profile{})
	return &DB{db}, nil
}

func generateHash(password string) (string, error) {
	pass, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", errors.Wrap(err, "cannot create hash")
	}
	return string(pass[:]), err
}
