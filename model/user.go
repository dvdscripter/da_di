package model

import (
	"github.com/gofrs/uuid"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
)

// User stores basic information about the user
type User struct {
	gorm.Model
	Email     string `gorm:"not null"`
	Password  string `gorm:"not null"`
	Forgot    string
	Profile   *Profile
	ProfileID uint
}

var ErrCreateUserExist = errors.New("user already exist")

func (u User) Public() User {
	return User{Email: u.Email, Profile: u.Profile}
}

func (db *DB) Login(u User) (bool, error) {

	dbUser, err := db.ReadUserByEmail(u.Email)
	if err != nil {
		return false, errors.Wrap(err, "cannot read pass from db")
	}

	err = bcrypt.CompareHashAndPassword([]byte(dbUser.Password), []byte(u.Password))
	if err != nil && err != bcrypt.ErrMismatchedHashAndPassword {
		return false, errors.Wrap(err, "wrong pass")
	} else if err == bcrypt.ErrMismatchedHashAndPassword {
		return false, nil
	}

	return true, nil
}

func (db *DB) CreateUser(u User) (User, error) {

	if _, err := db.ReadUserByEmail(u.Email); err == nil {
		return u, ErrCreateUserExist
	}

	pass, err := generateHash(u.Password)
	if err != nil {
		return u, err
	}
	u.Password = pass

	if u.Profile == nil {
		u.Profile = &Profile{}
	}
	u.Profile.Email = u.Email

	if err := db.Create(&u).Error; err != nil {
		return u, errors.Wrap(err, "cannot create user")
	}

	return u, nil
}

func (db *DB) ReadUser(id uint) (User, error) {
	u := User{}

	if err := db.Preload("Profile").First(&u, id).Error; err != nil {
		return u, errors.Wrap(err, "cannot read user from db")
	}

	return u, nil
}

func (db *DB) ReadUserByEmail(email string) (User, error) {
	u := User{}

	if err := db.Where("email = ?", email).Preload("Profile").First(&u).Error; err != nil {
		return u, errors.Wrap(err, "cannot read user from db")
	}

	return u, nil
}

func (db *DB) UpdateUser(id uint, u User) (User, error) {
	dbUser, err := db.ReadUser(id)
	if err != nil {
		return User{}, err
	}

	if u.Password != "" {
		pass, err := generateHash(u.Password)
		if err != nil {
			return User{}, err
		}
		u.Password = pass
	}
	if err := db.Model(&dbUser).Updates(u).Error; err != nil {
		return User{}, errors.Wrap(err, "cannot update user")
	}

	return dbUser, nil
}

func (db *DB) NewForgot(id uint) (string, error) {
	dbUser, err := db.ReadUser(id)
	if err != nil {
		return "", err
	}

	token, err := uuid.NewV4()
	tokenStr := token.String()
	if err != nil {
		return "", errors.Wrap(err, "cannot generate token")
	}

	if err := db.Model(&dbUser).Update("forgot", tokenStr).Error; err != nil {
		return "", errors.Wrap(err, "cannot gerate token")
	}

	return tokenStr, nil
}

func (db *DB) Profile(u *User) (Profile, error) {
	var p Profile
	if err := db.Model(u).Related(&p).Error; err != nil {
		return Profile{}, errors.Wrap(err, "cannot relate to profile")
	}
	return p, nil
}
