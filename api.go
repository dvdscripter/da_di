package main

import (
	"net/http"
	"testApp/model"

	"github.com/gorilla/mux"
	"github.com/pkg/errors"
)

func (app *App) login(w http.ResponseWriter, r *http.Request) (interface{}, error) {

	user := model.User{}
	if err := jsonFromRequest(&user, r); err != nil {
		return nil, err
	}

	status, err := app.storage.Login(user)
	if err != nil {
		return nil, err
	}

	if status {
		if err := app.setAuth(user.Email, true, w, r); err != nil {
			return nil, err
		}
	} else {
		return nil, errors.New("wrong credentials")
	}

	return user.Email, nil
}

func (app *App) createUser(w http.ResponseWriter, r *http.Request) (interface{}, error) {

	user := model.User{}
	if err := jsonFromRequest(&user, r); err != nil {
		return nil, err
	}

	newUser, err := app.storage.CreateUser(user)
	if err != nil {
		return nil, err
	}

	return newUser.Public(), app.setAuth(newUser.Email, true, w, r)
}

func (app *App) readUser(w http.ResponseWriter, r *http.Request) (interface{}, error) {

	vars := mux.Vars(r)
	email, ok := vars["email"]
	if ok && email == "" {
		return nil, errors.New("missing input email")
	}

	auth, err := app.getAuth(r)
	if err != nil {
		return nil, err
	}
	sessionEmail, err := app.getEmail(r)
	if err != nil {
		return nil, err
	}

	if !auth || email != sessionEmail {
		return nil, errors.New("you need to be authenticated with correct credentials")
	}

	user, err := app.storage.ReadUserByEmail(email)
	if err != nil {
		return nil, err
	}
	_, err = app.storage.Profile(&user)
	if err != nil {
		return nil, err
	}

	return user.Public(), nil
}

func (app *App) forgot(w http.ResponseWriter, r *http.Request) (interface{}, error) {

	if auth, err := app.getAuth(r); err == nil && auth {
		return nil, errors.New("only unauthenticated access")
	} else if err != nil {
		return nil, err
	}

	user := model.User{}
	if err := jsonFromRequest(&user, r); err != nil {
		return nil, err
	}

	dbUser, err := app.storage.ReadUserByEmail(user.Email)
	if err != nil {
		return nil, err
	}

	if dbUser.Forgot == user.Forgot {
		user.Forgot = ""
		uu, err := app.storage.UpdateUser(dbUser.ID, user)
		return uu.Public(), err
	}

	return nil, errors.New("token mismatch")
}

func (app *App) createForgot(w http.ResponseWriter, r *http.Request) (interface{}, error) {

	if auth, err := app.getAuth(r); err == nil && auth {
		return nil, errors.New("only unauthenticated access")
	} else if err != nil {
		return nil, err
	}

	user := model.User{}
	if err := jsonFromRequest(&user, r); err != nil {
		return nil, err
	}

	dbUser, err := app.storage.ReadUserByEmail(user.Email)
	if err != nil {
		return nil, err
	}

	token, err := app.storage.NewForgot(dbUser.ID)
	if err != nil {
		return nil, err
	}

	if err := app.sendEmail(dbUser.Email, token); err != nil {
		return nil, err
	}

	return token, nil
}

func (app *App) updateProfile(w http.ResponseWriter, r *http.Request) (interface{}, error) {

	vars := mux.Vars(r)
	email, ok := vars["email"]
	if ok && email == "" {
		return nil, errors.New("missing input email")
	}

	auth, err := app.getAuth(r)
	if err != nil {
		return nil, err
	}
	sessionEmail, err := app.getEmail(r)
	if err != nil {
		return nil, err
	}

	profile := model.Profile{}
	if err := jsonFromRequest(&profile, r); err != nil {
		return nil, err
	}

	if !auth || email != sessionEmail {
		return nil, errors.New("you need to be authenticated with correct credentials")
	}

	user, err := app.storage.UpdateProfile(email, profile)
	if err != nil {
		return nil, err
	}

	return user.Public(), app.setAuth(user.Email, true, w, r)
}

func (app *App) loginOAuth(w http.ResponseWriter, r *http.Request) (interface{}, error) {

	if auth, err := app.getAuth(r); err == nil && auth {
		return nil, errors.New("only unauthenticated access")
	} else if err != nil {
		return nil, err
	}

	idtoken := r.FormValue("idtoken")

	user, err := app.verifyToken(idtoken)
	if err != nil {
		return nil, err
	}

	user, err = app.storage.CreateUser(user)
	if err != nil && err != model.ErrCreateUserExist {
		return nil, err
	}

	if err := app.setAuth(user.Email, true, w, r); err != nil {
		return nil, err
	}

	return user.Public(), nil

}
