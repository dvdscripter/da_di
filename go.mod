module testApp

require (
	4d63.com/gochecknoglobals v0.0.0-20180908201037-5090db600a84 // indirect
	4d63.com/gochecknoinits v0.0.0-20180528051558-14d5915061e5 // indirect
	github.com/alecthomas/gocyclo v0.0.0-20150208221726-aa8f8b160214 // indirect
	github.com/alexflint/go-arg v0.0.0-20181120184500-fb7d95b61ba8 // indirect
	github.com/alexflint/go-scalar v1.0.0 // indirect
	github.com/alexkohler/nakedret v0.0.0-20171106223215-c0e305a4f690 // indirect
	github.com/client9/misspell v0.3.4 // indirect
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gofrs/uuid v3.1.0+incompatible
	github.com/golang/lint v0.0.0-20181026193005-c67002cb31c3 // indirect
	github.com/gordonklaus/ineffassign v0.0.0-20180909121442-1003c8bd00dc // indirect
	github.com/gorilla/mux v1.6.2
	github.com/gorilla/sessions v1.1.3
	github.com/jgautheron/goconst v0.0.0-20170703170152-9740945f5dcb // indirect
	github.com/jinzhu/gorm v1.9.2
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	github.com/kisielk/errcheck v1.1.0 // indirect
	github.com/lestrrat-go/jwx v0.0.0-20180928232350-0d477e6a1f0e
	github.com/lestrrat-go/pdebug v0.0.0-20180220043849-39f9a71bcabe // indirect
	github.com/mdempsky/maligned v0.0.0-20180708014732-6e39bd26a8c8 // indirect
	github.com/mdempsky/unconvert v0.0.0-20180703203632-1a9a0a0a3594 // indirect
	github.com/mibk/dupl v1.0.0 // indirect
	github.com/opennota/check v0.0.0-20180911053232-0c771f5545ff // indirect
	github.com/pelletier/go-toml v1.2.0
	github.com/pkg/errors v0.8.0
	github.com/satori/go.uuid v1.2.0
	github.com/securego/gosec v0.0.0-20181204082206-24e3094d2a58 // indirect
	github.com/stripe/safesql v0.0.0-20171221195208-cddf355596fe // indirect
	github.com/tsenart/deadcode v0.0.0-20160724212837-210d2dc333e9 // indirect
	github.com/walle/lll v0.0.0-20160702150637-8b13b3fbf731 // indirect
	golang.org/x/crypto v0.0.0-20181203042331-505ab145d0a9
	golang.org/x/lint v0.0.0-20181026193005-c67002cb31c3 // indirect
	golang.org/x/tools v0.0.0-20181207222222-4c874b978acb // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
	honnef.co/go/tools v0.0.0-20180920025451-e3ad64cb4ed3 // indirect
	mvdan.cc/interfacer v0.0.0-20180901003855-c20040233aed // indirect
	mvdan.cc/lint v0.0.0-20170908181259-adc824a0674b // indirect
	mvdan.cc/unparam v0.0.0-20181201214637-68701730a1d7 // indirect
)
