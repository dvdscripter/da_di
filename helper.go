package main

import (
	"encoding/json"
	"html/template"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/pkg/errors"
)

// oauthToken contains information needed for validation and profile scope
type oauthToken struct {
	Aud   string
	Iss   string
	Exp   int64
	Email string
	Name  string
}

func jsonFromRequest(dst interface{}, r *http.Request) error {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}
	if !json.Valid(body) {
		return errors.Errorf("Invalid body content")
	}

	if err := json.Unmarshal(body, &dst); err != nil {
		return errors.Wrap(err, "cannot unmarshal json body")
	}
	return nil
}

func jsonResp(src interface{}, err error, w http.ResponseWriter) error {
	type resp struct {
		Error  string      `json:"error"`
		Result interface{} `json:"result"`
	}
	r := resp{Result: src}

	if err != nil {
		r.Error = err.Error()
	}

	return json.NewEncoder(w).Encode(r)
}

// dynamic rendering of template layout with correct content and data
func render(w http.ResponseWriter, page string, data interface{}) {
	w.Header().Set("Content-Type", "text/html")
	if page == "favicon.ico" {
		return
	}
	templates, err := template.ParseFiles("templates/layout.view.html", "templates/"+page+".view.html")
	if err != nil {
		panic(err)
	}

	if err := templates.ExecuteTemplate(w, "layout", &data); err != nil {
		panic(err)
	}
}

func (t oauthToken) valid(c Config) bool {
	if t.Aud == c.Google.ClientID &&
		t.Iss == "accounts.google.com" ||
		t.Iss == "https://accounts.google.com" &&
			time.Now().Before(time.Unix(t.Exp, 0)) {
		return true
	}

	return false
}
