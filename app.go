package main

import (
	"log"
	"net/http"
	"encoding/json"
	"testApp/model"

	"github.com/gorilla/sessions"
	"github.com/pkg/errors"
	"github.com/lestrrat-go/jwx/jwk"
	"github.com/lestrrat-go/jwx/jws"
	gomail "gopkg.in/gomail.v2"
)
// App used primary for dependency injection
type App struct {
	logger       *log.Logger
	storage      model.Storage
	config       *Config
	sessionStore *sessions.CookieStore
}
// Config holds each configuration relevant to App
type Config struct {
	Bind           string
	SessionAuthKey string
	SessionEncKey  string
	DSN            string
	WWW            string
	SMTP           SMTP
	Google         Google
}
// Google subgroup config with MAP KEY and OAuth Client ID and Secret
type Google struct {
	Map          string
	ClientID     string
	ClientSecret string
}
// SMTP subgroup to send forgot password e-mails
type SMTP struct {
	Host     string
	Port     int
	Username string
	Password string
}

// custom handler because marshaling and error handling gets old very fast
type appHandler func(w http.ResponseWriter, r *http.Request) (interface{}, error)

// logger and json encode middleware
func (app *App) errorMiddleware(fn appHandler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		res, err := fn(w, r)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			app.logger.Printf("%+v\n", err)
		}
		w.Header().Set("Content-Type", "application/json")
		jsonResp(res, err, w)
	})
}

func (app *App) setAuth(email string, status bool, w http.ResponseWriter, r *http.Request) error {
	session, err := app.sessionStore.Get(r, "testApp")
	if err != nil {
		return errors.Wrap(err, "cannot read session")
	}

	session.Values["auth"] = status
	session.Values["email"] = email
	return session.Save(r, w)
}

func (app *App) getAuth(r *http.Request) (bool, error) {
	session, err := app.sessionStore.Get(r, "testApp")
	if err != nil {
		return false, errors.Wrap(err, "cannot read session")
	}
	if auth, ok := session.Values["auth"].(bool); ok && auth {
		return true, nil
	}

	return false, nil
}

func (app *App) getEmail(r *http.Request) (string, error) {
	session, err := app.sessionStore.Get(r, "testApp")
	if err != nil {
		return "", errors.Wrap(err, "cannot read session")
	}
	if email, ok := session.Values["email"].(string); ok {
		return email, nil
	}

	return "", errors.New("email field not found at session")
}

func (app *App) sendEmail(email, token string) error {
	m := gomail.NewMessage()
	m.SetHeader("From", "no-reply@testapp.com")
	m.SetHeader("To", email)
	m.SetHeader("Subject", "Reset Password!")
	m.SetBody("text/html", "Hello reset your password here: <a href="+
		app.config.WWW+"forgot/"+email+"/"+token+">Click Here</a>!")

	d := gomail.NewDialer(app.config.SMTP.Host, app.config.SMTP.Port,
		app.config.SMTP.Username, app.config.SMTP.Password)

	return d.DialAndSend(m)
}

func (app *App) endSession(w http.ResponseWriter, r *http.Request) error {
	session, err := app.sessionStore.Get(r, "testApp")
	if err != nil {
		return errors.Wrap(err, "cannot read session")
	}
	session.Options.MaxAge = -1
	return session.Save(r, w)
}

func (app *App) verifyToken(token string) (model.User, error) {
	var tokenInfo oauthToken
	
	set, err := jwk.Fetch("https://www.googleapis.com/oauth2/v3/certs")	
	if err != nil {
		return model.User{}, errors.Wrap(err, "cannot fetch google cert keys")
	}
	keys := set.Keys

	for _, key := range keys {
		verified, err := jws.VerifyWithJWK([]byte(token), key)
		if err != nil {
			continue
		}
		if err := json.Unmarshal(verified, &tokenInfo); err != nil {
			return model.User{}, errors.Wrap(err, "cannot interpret token info")
		}
		if tokenInfo.valid(*app.config) {
			return model.User{Email: tokenInfo.Email, Profile: &model.Profile{
				Name: tokenInfo.Name,
				Email: tokenInfo.Email,
			}}, nil
		}

	}
	
	return model.User{}, errors.New("cannot check this token")
}
